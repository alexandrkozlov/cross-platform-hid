using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using HomeInteriorDesign.RawModel;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace HomeInteriorDesignORMTest
{
	public partial class DocumentsViewController : UITableViewController
	{
		private List<PlanDocument> planDocuments;
		private AddDocumentAlertViewDelegate alertViewDelegate;

		public DocumentsViewController (IntPtr handle) : base (handle)
		{
			this.planDocuments = new List<PlanDocument>();
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			this.NavigationItem.RightBarButtonItem = new UIBarButtonItem(UIBarButtonSystemItem.Add, AddDocument);
		}
		
		private void AddDocument(object sender, EventArgs e)
		{
			this.alertViewDelegate = new AddDocumentAlertViewDelegate();
			this.alertViewDelegate.DocumentAdded += AddedDocument;

			UIAlertView alertView = new UIAlertView("Enter document name:", String.Empty, this.alertViewDelegate, "Cancel", "Add");
			alertView.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			alertView.Show();
		}

		private void AddedDocument(object sender, AddDocumentEventArg e)
		{
			this.planDocuments.Add(e.Document);
			this.TableView.ReloadData();
			this.alertViewDelegate = null;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			this.ReloadDocuments();
		}

		private void ReloadDocuments()
		{
			List<PlanDocument> documents = new List<PlanDocument>();

			string documentsDirectory = NSSearchPath.GetDirectories(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User).First();
			NSError error = null;
			string[] documentNames = NSFileManager.DefaultManager.GetDirectoryContent(documentsDirectory, out error);
			foreach (string documentName in documentNames)
			{
				string documentPath = Path.Combine(documentsDirectory, documentName);
				documents.Add(new PlanDocument(documentPath));
			}

			this.planDocuments = documents.OrderBy(doc => doc.Path).ToList();
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			UITableViewCell cell = (UITableViewCell)sender;
			NSIndexPath indexPath = this.TableView.IndexPathForCell(cell);
			PlanDocumentViewController planViewController = (PlanDocumentViewController)segue.DestinationViewController;
			planViewController.RawPlan = this.planDocuments[indexPath.Row].Plan;
		}

		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return this.planDocuments.Count();
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell(new NSString("Cell"), indexPath);
			cell.TextLabel.Text = this.planDocuments[indexPath.Row].Name;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			return cell;
		}

		public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
		{
			if (editingStyle == UITableViewCellEditingStyle.Delete)
			{
				PlanDocument document = this.planDocuments[indexPath.Row];

				NSError error = null;
				if (NSFileManager.DefaultManager.Remove(document.Path, out error))
				{
					this.planDocuments.Remove(document);
					this.TableView.ReloadData();
				}
			}
		}

		public class AddDocumentEventArg : EventArgs
		{
			public PlanDocument Document { get; private set; }

			public AddDocumentEventArg(PlanDocument document)
			{
				this.Document = document;
			}
		}

		private class AddDocumentAlertViewDelegate : UIAlertViewDelegate
		{
			public event EventHandler<AddDocumentEventArg> DocumentAdded;

			public override void Clicked(UIAlertView alertview, int buttonIndex)
			{
				string documentName = alertview.GetTextField(0).Text;
				string documentsDirectory = NSSearchPath.GetDirectories(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User).First();
				PlanDocument newDocument = new PlanDocument(Path.Combine(documentsDirectory, documentName));
				newDocument.Plan.Name = documentName;
				newDocument.Save();

				if (this.DocumentAdded != null)
				{
					this.DocumentAdded(this, new AddDocumentEventArg(newDocument));
				}
			}
		}
	}
}

