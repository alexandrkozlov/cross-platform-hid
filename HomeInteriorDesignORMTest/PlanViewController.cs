using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using HomeInteriorDesign.GeometryModel;

namespace HomeInteriorDesignORMTest
{
	public partial class PlanViewController : UIViewController
	{
		private Plan plan;
		private Dictionary<Room, RoomViewController> roomViewControllers;

		public PlanViewController() : base(null, null)
		{
			this.roomViewControllers = new Dictionary<Room, RoomViewController>();
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			this.View.BackgroundColor = UIColor.Red;
		}

		public Plan Plan
		{
			get
			{
				return this.plan;
			}
			set
			{
				if (this.plan != null)
				{
					foreach (Room room in this.plan.Rooms)
					{
						this.RemoveRoom(room);
					}

					this.Plan.RoomAdded -= RoomAdded;
					this.Plan.RoomRemoved -= RoomRemoved;
				}

				this.plan= value;

				if (this.plan != null)
				{
					foreach (Room room in this.plan.Rooms)
					{
						this.AddRoom(room);
					}

					this.Plan.RoomAdded += RoomAdded;
					this.Plan.RoomRemoved += RoomRemoved;
				}
			}
		}

		private void RoomAdded(object sender, RoomEventArgs e)
		{
			this.AddRoom(e.Room);
		}

		private void RoomRemoved(object sender, RoomEventArgs e)
		{
			this.RemoveRoom(e.Room);
		}

		private void AddRoom(Room room)
		{
			RoomViewController roomViewController = new RoomViewController();
			roomViewController.Room = room;
			this.roomViewControllers[room] = roomViewController;
			this.View.AddSubview(roomViewController.View);
		}

		private void RemoveRoom(Room room)
		{
			RoomViewController roomViewController = this.roomViewControllers[room];
			roomViewController.View.RemoveFromSuperview();
			this.roomViewControllers.Remove(room);
		}
	}
}

