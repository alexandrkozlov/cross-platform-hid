using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using HomeInteriorDesign.RawModel.Abstract;
using HomeInteriorDesign.GeometryModel;

namespace HomeInteriorDesignORMTest
{
	public partial class PlanDocumentViewController : UIViewController
	{
		private Plan plan;
		private PlanViewController planViewController;

		public PlanDocumentViewController (IntPtr handle) : base (handle)
		{

		}

		public IPlan RawPlan { get; set; }

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			this.Title = this.RawPlan.Name;

			this.plan = new Plan();
			this.planViewController = new PlanViewController();
			this.planViewController.Plan = this.plan;
			this.planViewController.View.Frame = this.View.Bounds;
			this.planViewController.View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
			this.View.AddSubview(this.planViewController.View);
		}

		partial void AddRoom (MonoTouch.Foundation.NSObject sender)
		{
			Room room = new Room();
			room.Points = new Point[] { 
				new Point(100, 100), 
				new Point(100, 200), 
				new Point(200, 200), 
				new Point(200, 100) 
			};
			this.plan.AddRoom(room);
		}
	}
}

