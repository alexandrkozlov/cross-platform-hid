using System;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;
using HomeInteriorDesign.GeometryModel;
using MonoTouch.CoreGraphics;

namespace HomeInteriorDesignORMTest
{
	public partial class RoomViewController : UIViewController
	{
		private Room room;
		private CAShapeLayer shapeLayer;

		public RoomViewController() : base(null, null)
		{
		}

		public override void LoadView()
		{
			base.LoadView();

			this.shapeLayer = new CAShapeLayer();
			this.View.Layer.AddSublayer(this.shapeLayer);
			this.View.BackgroundColor = UIColor.Green;
		}

		public Room Room
		{
			get
			{
				return this.room;
			}
			set
			{
				if (this.room != null)
				{
					this.Room.PointsChanged -= RoomPointsChanged;
				}

				this.room = value;

				this.UpdateView();

				if (this.room != null)
				{
					this.Room.PointsChanged += RoomPointsChanged;
				}
			}
		}

		private void RoomPointsChanged(object sender, EventArgs e)
		{
			this.UpdateView();
		}

		private void UpdateView()
		{
			System.Drawing.RectangleF frame = new System.Drawing.RectangleF();
			CGPath path = new CGPath();

			if (this.Room != null)
			{
				for (int i = 0; i < this.Room.Points.Count(); i++)
				{
					if (i == 0)
					{
						path.MoveToPoint(new System.Drawing.PointF((float)this.Room.Points.ElementAt(i).X, (float)this.Room.Points.ElementAt(i).Y));
					}
					else
					{
						path.AddLineToPoint(new System.Drawing.PointF((float)this.Room.Points.ElementAt(i).X, (float)this.Room.Points.ElementAt(i).Y));
					}
				}
				path.CloseSubpath();

				frame = path.BoundingBox;
			}

			this.View.Frame = frame;
			this.shapeLayer.Path = path;
		}
	}
}

