using System;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;
using HomeInteriorDesign.RawModel;
using Java.IO;

namespace HomeInteriorDesign.Android
{
	public class DocumentsAdapter : BaseAdapter
	{
		private File documentsDirectory;
		private List<PlanDocument> documents;

		public DocumentsAdapter(File documentsDirectory)
		{
			this.documentsDirectory = documentsDirectory;
			this.documents = new List<PlanDocument>();
			foreach (File documentFile in this.documentsDirectory.ListFiles())
			{
				this.documents.Add(new PlanDocument(documentFile.AbsolutePath));
			}
		}

		#region implemented abstract members of BaseAdapter

		public override Java.Lang.Object GetItem(int position)
		{
			throw new NotImplementedException();
		}

		public override long GetItemId(int position)
		{
			throw new NotImplementedException();
		}

		public override global::Android.Views.View GetView(int position, global::Android.Views.View convertView, global::Android.Views.ViewGroup parent)
		{
			TextView textView = new TextView(parent.Context);
			textView.Text = this.documents[position].Name;
			return textView;
		}

		public override int Count
		{
			get
			{
				return this.documents.Count;
			}
		}

		#endregion
	}
}

