using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace HomeInteriorDesign.GeometryModel
{
	public class Room
	{
		private ReadOnlyCollection<Point> points;

		public IEnumerable<Point> Points
		{
			get
			{
				return this.points;
			}
			set
			{
				if (value == null)
					throw new ArgumentNullException("value");

				if (value.Count() < 3)
					throw new ArgumentOutOfRangeException("Room must have at least 3 points.");

				this.points = new ReadOnlyCollection<Point>(value);
				this.OnPointsChanged();
			}
		}

		public event EventHandler PointsChanged;

		private void OnPointsChanged()
		{
			if (this.PointsChanged != null)
			{
				this.PointsChanged(this, EventArgs.Empty);
			}
		}
	}
}

