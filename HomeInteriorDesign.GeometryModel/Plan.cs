using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace HomeInteriorDesign.GeometryModel
{
	public class RoomEventArgs : EventArgs
	{
		public Room Room { get; private set; }

		public RoomEventArgs(Room room)
		{
			this.Room = room;
		}
	}

	public class Plan
	{
		private HashSet<Room> rooms = new HashSet<Room>();

		public ReadOnlyCollection<Room> Rooms
		{
			get
			{
				return new ReadOnlyCollection<Room>(new List<Room>(this.rooms)); 
			}
		}

		public void AddRoom(Room room)
		{
			if (room == null)
				throw new ArgumentNullException("room");

			if (this.rooms.Add(room))
			{
				this.OnRoomAdded(new RoomEventArgs(room));
			}
		}

		public void RemoveRoom(Room room)
		{
			if (room == null)
				throw new ArgumentNullException("room");

			if (this.rooms.Remove(room))
			{
				this.OnRoomRemoved(new RoomEventArgs(room));
			}
		}

		public event EventHandler<RoomEventArgs> RoomAdded;

		private void OnRoomAdded(RoomEventArgs e)
		{
			if (this.RoomAdded != null)
			{
				this.RoomAdded(this, e);
			}
		}

		public event EventHandler<RoomEventArgs> RoomRemoved;

		private void OnRoomRemoved(RoomEventArgs e)
		{
			if (this.RoomRemoved != null)
			{
				this.RoomRemoved(this, e);
			}
		}
	}
}

