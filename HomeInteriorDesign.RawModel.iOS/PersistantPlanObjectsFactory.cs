using System;
using HomeInteriorDesign.RawModel.Abstract;

namespace HomeInteriorDesign.RawModel
{
	public class PersistantPlanObjectsFactory : IPlanObjectsFactory
	{
		private readonly PlanObjectsRepository repository;

		public PersistantPlanObjectsFactory(PlanObjectsRepository repository)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");

			this.repository = repository;
		}

		public IRoom CreateRoom()
		{
			RawRoom room = new RawRoom();
			this.repository.AddObject(room);
			return room;
		}

		public IRoomPoint CreateRoomPoint()
		{
			RawRoomPoint roomPoint = new RawRoomPoint();
			this.repository.AddObject(roomPoint);
			return roomPoint;
		}
	}
}

