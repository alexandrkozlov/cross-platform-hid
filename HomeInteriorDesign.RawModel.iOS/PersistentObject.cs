using System;
using SQLite;

namespace HomeInteriorDesign.RawModel
{
	public class PersistentObject
	{
		[PrimaryKey, AutoIncrement, Column("id")]
		public int Id { get; set; }

		internal bool NeedsUpdate { get; set; }
		internal bool NeedsDelete { get; set; }

		internal void ResetChanges()
		{
			this.NeedsUpdate = false;
			this.NeedsDelete = false;
		}
	}
}

