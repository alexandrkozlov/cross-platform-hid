using System;
using HomeInteriorDesign.RawModel.Abstract;

namespace HomeInteriorDesign.RawModel
{
	public class PlanObjectsFactory : IPlanObjectsFactory
	{
		public IRoom CreateRoom()
		{
			return new RawRoom();
		}

		public IRoomPoint CreateRoomPoint()
		{
			return new RawRoomPoint();
		}
	}
}

