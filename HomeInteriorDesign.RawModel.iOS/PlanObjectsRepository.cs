using System;
using System.Linq;
using System.Collections.Generic;
using SQLite;
using HomeInteriorDesign.RawModel.Abstract;
using System.IO;

namespace HomeInteriorDesign.RawModel
{
	public class PlanObjectsRepository
	{
		private SQLiteConnection connection;
		private RawPlan plan;
		private HashSet<PersistentObject> storedObjects;
		private HashSet<PersistentObject> memoryObjects;

		public PlanObjectsRepository(SQLiteConnection connection)
		{
			this.connection = connection;
			this.storedObjects = new HashSet<PersistentObject>();
			this.memoryObjects = new HashSet<PersistentObject>();
		}

		public RawPlan Plan
		{
			get
			{
				if (this.plan == null)
				{
					IEnumerable<RawPlan> plans = this.connection.Table<RawPlan>();

					if (plans.Count() == 0)
					{
						this.plan = new RawPlan();
						this.connection.Insert(this.plan, typeof(RawPlan));
						this.storedObjects.Add(this.plan);
					}
					else
					{
						if (plans.Count() > 1)
							throw new Exception("Plan database must contain only one plan.");

						this.plan = plans.Single();
						this.storedObjects.Add(plan);

						foreach (RawRoom room in this.connection.Table<RawRoom>())
						{
							this.plan.AddRoom(room);
							this.storedObjects.Add(room);

							foreach (RawRoomPoint roomPoint in this.connection.Table<RawRoomPoint>().Where(pt => pt.RoomId == room.Id))
							{
								room.AddRoomPoint(roomPoint);
								this.storedObjects.Add(roomPoint);
							}
						}
					}
				}

				return this.plan;
			}
		}

		public void AddObject(PersistentObject obj)
		{
			this.memoryObjects.Add(obj);		
		}

		public void Save()
		{
			HashSet<PersistentObject> deletedRoomPoints = new HashSet<PersistentObject>();
			foreach (PersistentObject obj in this.storedObjects)
			{
				if (obj.NeedsUpdate)
				{
					if (obj.NeedsDelete)
					{
						this.connection.Delete<RawRoomPoint>(obj.Id);
						deletedRoomPoints.Add(obj);
						obj.ResetChanges();
					} 
					else if (obj.NeedsUpdate)
					{
						this.connection.Update(obj);
						obj.ResetChanges();
					}
				}
			}

			this.storedObjects.ExceptWith(deletedRoomPoints);

			foreach (PersistentObject obj in this.memoryObjects)
			{
				if (!obj.NeedsDelete)
				{
					this.connection.Insert(obj);
					this.storedObjects.Add(obj);
				}
			}

			this.memoryObjects.Clear();
		}
	}
}

