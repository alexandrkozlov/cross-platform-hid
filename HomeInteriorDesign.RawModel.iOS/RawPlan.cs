using System;
using System.Collections.Generic;
using HomeInteriorDesign.RawModel.Abstract;
using SQLite;

namespace HomeInteriorDesign.RawModel
{
	[Table("plan")]
	public class RawPlan : PersistentObject, IPlan
	{
		private IPlanObjectsFactory objectsFactory;

		public RawPlan()
		{
			this.objectsFactory = new PlanObjectsFactory();
		}

		[Ignore]
		public IPlanObjectsFactory ObjectsFactory
		{
			get
			{
				return this.objectsFactory;
			}
			internal set
			{
				this.objectsFactory = value; 
			}
		}


		#region Name

		private string name;

		[Column("name")]
		public string Name
		{
			get
			{ 
				return this.name;
			}
			set
			{
				this.name = value;
				this.NeedsUpdate = true; 
			}
		}

		#endregion Name


		#region Rooms

		private List<RawRoom> rooms = new List<RawRoom>();

		public void AddRoom(RawRoom room)
		{
			room.Plan = this;
		}

		public void RemoveRoom(RawRoom room)
		{
			room.Plan = null;
		}

		public IEnumerable<RawRoom> Rooms
		{
			get
			{
				return this.rooms;
			}
		}

		internal void AddRoomToCollection(RawRoom roomPoint)
		{
			this.rooms.Add(roomPoint);
		}

		internal void RemoveRoomFromCollection(RawRoom roomPoint)
		{
			this.rooms.Remove(roomPoint);
		}

		void IPlan.AddRoom(IRoom room)
		{
			this.AddRoom((RawRoom)room);
		}

		void IPlan.RemoveRoom(IRoom room)
		{
			this.RemoveRoom((RawRoom)room);
		}

		IEnumerable<IRoom> IPlan.Rooms
		{
			get
			{
				return (IEnumerable<IRoom>)this.rooms;
			}
		}

		#endregion Rooms
	}
}

