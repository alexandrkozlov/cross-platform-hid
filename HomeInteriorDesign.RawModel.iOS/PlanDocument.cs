using System;
using System.Collections.Generic;
using System.Linq;
using HomeInteriorDesign.RawModel.Abstract;
using SQLite;

namespace HomeInteriorDesign.RawModel
{
	public class PlanDocument
	{
		private readonly SQLiteConnection connection;
		private readonly PlanObjectsRepository repository;
		private readonly PersistantPlanObjectsFactory factory;

		public PlanDocument(string path)
		{
			this.connection = new SQLiteConnection(path);
			this.connection.CreateTable<RawPlan>();
			this.connection.CreateTable<RawRoom>();
			this.connection.CreateTable<RawRoomPoint>();

			this.repository = new PlanObjectsRepository(this.connection);
			this.factory = new PersistantPlanObjectsFactory(this.repository);
			this.repository.Plan.ObjectsFactory = this.factory;
		}

		public string Path
		{
			get
			{
				return this.connection.DatabasePath; 
			}
		}

		public string Name
		{
			get
			{
				return System.IO.Path.GetFileName(this.Path);
			}
		}

		public SQLiteConnection Connection
		{
			get
			{
				return this.connection; 
			}
		}

		public RawPlan Plan
		{
			get
			{
				return this.repository.Plan; 
			}
		}

		public void Save()
		{
			this.repository.Save();
		}
	}
}

