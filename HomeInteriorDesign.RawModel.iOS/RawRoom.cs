using System;
using HomeInteriorDesign.RawModel.Abstract;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace HomeInteriorDesign.RawModel
{
	[Table("rooms")]
	public class RawRoom : PersistentObject, IRoom
	{
		#region Plan

		[Column("plan_id")]
		public int PlanId { get; set; }

		private RawPlan plan;

		[Ignore]
		public RawPlan Plan
		{
			get
			{
				return this.plan;
			}
			set
			{
				if (value != this.Plan)
				{
					if (this.plan != null)
					{
						this.plan.RemoveRoomFromCollection(this);
					}

					this.plan = value;

					if (this.plan != null)
					{
						this.PlanId = this.plan.Id;
						this.plan.AddRoomToCollection(this);
						this.NeedsUpdate = true;
					}
					else
					{
						this.PlanId = 0;
						this.NeedsDelete = true;
					}
				}
			}
		}

		IPlan IRoom.Plan
		{
			get
			{
				return this.Plan;
			}
			set
			{
				this.Plan = (RawPlan)value;
			}
		}

		#endregion Plan


		#region Room Points

		private List<RawRoomPoint> roomPoints = new List<RawRoomPoint>();

		public void AddRoomPoint(RawRoomPoint roomPoint)
		{
			roomPoint.Room = this;
		}

		public void RemoveRoomPoint(RawRoomPoint roomPoint)
		{
			roomPoint.Room = null;
		}

		internal void AddRoomPointToCollection(RawRoomPoint roomPoint)
		{
			this.roomPoints.Add(roomPoint);
		}

		internal void RemoveRoomPointFromCollection(RawRoomPoint roomPoint)
		{
			this.roomPoints.Remove(roomPoint);
		}

		void IRoom.AddRoomPoint(IRoomPoint roomPoint)
		{
			this.AddRoomPoint((RawRoomPoint)roomPoint);
		}

		void IRoom.RemoveRoomPoint(IRoomPoint roomPoint)
		{
			this.RemoveRoomPoint((RawRoomPoint)roomPoint);
		}

		IEnumerable<IRoomPoint> IRoom.RoomPoints 
		{
			get 
			{
				return (IEnumerable<IRoomPoint>)this.roomPoints;
			}
		}

		#endregion Room Points
	}
}

