using System;
using HomeInteriorDesign.RawModel.Abstract;
using SQLite;

namespace HomeInteriorDesign.RawModel
{
	[Table("room_points")]
	public class RawRoomPoint : PersistentObject, IRoomPoint
	{
		[Column("x")]
		public double X { get; set; }

		[Column("y")]
		public double Y { get; set; }

		#region Room

		[Column("room_id")]
		public int RoomId { get; set; }

		private RawRoom room;

		[Ignore]
		public RawRoom Room
		{
			get
			{
				return this.room;
			}
			set
			{
				if (value != this.Room)
				{
					if (this.room != null)
					{
						this.room.RemoveRoomPointFromCollection(this);
					}

					this.room = value;

					if (this.room != null)
					{
						this.RoomId = this.room.Id;
						this.room.AddRoomPointToCollection(this);
						this.NeedsUpdate = true;
					}
					else
					{
						this.RoomId = 0;
						this.NeedsDelete = true;
					}
				}
			}
		}

		IRoom IRoomPoint.Room 
		{
			get 
			{
				return this.room;
			}
			set 
			{
				this.room = (RawRoom)room;
			}
		}

		#endregion Room
	}
}

