using System;
using System.Collections.Generic;

namespace HomeInteriorDesign.RawModel.Abstract
{
	public interface IPlan
	{
		string Name { get; }
		IEnumerable<IRoom> Rooms { get; }
		void AddRoom(IRoom room);
		void RemoveRoom(IRoom room);

		IPlanObjectsFactory ObjectsFactory { get; }
	}
}

