using System;

namespace HomeInteriorDesign.RawModel.Abstract
{
	public interface IPlanObjectsFactory
	{
		IRoom CreateRoom();
		IRoomPoint CreateRoomPoint();
	}
}

