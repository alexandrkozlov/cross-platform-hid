using System;
using System.Collections.Generic;

namespace HomeInteriorDesign.RawModel.Abstract
{
	public interface IRoom
	{
		IPlan Plan { get; set; }
		IEnumerable<IRoomPoint> RoomPoints { get; }
		void AddRoomPoint(IRoomPoint roomPoint);
		void RemoveRoomPoint(IRoomPoint roomPoint);
	}
}

