using System;

namespace HomeInteriorDesign.RawModel.Abstract
{
	public interface IRoomPoint
	{
		IRoom Room { get; set; }
		double X { get; set; }
		double Y { get; set; }
	}
}

